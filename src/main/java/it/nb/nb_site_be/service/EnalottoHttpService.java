package it.nb.nb_site_be.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface EnalottoHttpService {

	@GET("risultati/{year}")
	Call<ResponseBody> listByYear(@Path("year") String year);
}
