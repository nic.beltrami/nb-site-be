package it.nb.nb_site_be.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.springframework.stereotype.Service;

import it.nb.nb_site_be.entity.EnalottoExtraction;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.Retrofit;

@Service
public class EnalottoServiceImpl implements EnalottoService {

	private static final String baseUrl = "https://www.superenalotto.com/";

	EnalottoHttpService enalottoHttpService;

	public EnalottoServiceImpl() {
		super();
		Retrofit retrofit = new Retrofit.Builder().baseUrl(baseUrl).build();

		enalottoHttpService = retrofit.create(EnalottoHttpService.class);
	}

	@Override
	public List<EnalottoExtraction> list(String year) {
		List<EnalottoExtraction> enalottoExtraction = new ArrayList<>();
		try {
			Response<ResponseBody> response = enalottoHttpService.listByYear(year).execute();
			Document doc = Jsoup.parse(response.body().string());
			Element table = doc.getElementById("t1");
			table.childNodes().forEach(cn -> {
				if (cn instanceof Element) {
					for (int i = 2; i < cn.childNodes().size(); i++) {
						Node extraction = cn.childNodes().get(i);
						if (extraction instanceof Element) {
							String extractionDate = ((Element) extraction).getElementsByTag("th").text();
							List<Integer> extractionNumbers = ((Element) extraction).getElementsByClass("ball-24px")
									.stream().map(Element::text).map(el -> Integer.parseInt(el))
									.collect(Collectors.toList());
							Integer extractionNumberJolly = Integer
									.parseInt(((Element) extraction).getElementsByClass("jolly-24px").first().text());
							Integer extractionNumberSuperstar = Integer.parseInt(
									((Element) extraction).getElementsByClass("superstar-24px").first().text());
							enalottoExtraction
									.add(new EnalottoExtraction().date(extractionDate).numbers(extractionNumbers)
											.jolly(extractionNumberJolly).superstar(extractionNumberSuperstar));
						}
					}
				}
			});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return enalottoExtraction;
	}

}
