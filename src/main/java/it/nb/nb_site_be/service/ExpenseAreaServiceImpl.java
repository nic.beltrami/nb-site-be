package it.nb.nb_site_be.service;

import it.nb.nb_site_be.entity.ExpenseArea;
import it.nb.nb_site_be.repository.ExpenseAreaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ExpenseAreaServiceImpl implements ExpenseAreaService {

    ExpenseAreaRepository repository;

    @Autowired
    public ExpenseAreaServiceImpl(ExpenseAreaRepository repository) {
        this.repository = repository;
    }


    @Override
    public List<ExpenseArea> findAll() {
        return repository.findAll();
    }

    @Override
    public ExpenseArea findById(Integer id) {
        Optional<ExpenseArea> expenseArea = repository.findById(id);
        if (!expenseArea.isPresent()) {
            throw new IllegalStateException();
        }
        return expenseArea.get();
    }

    @Override
    public void save(ExpenseArea expenseArea) {
        if (expenseArea == null) {
            throw new IllegalStateException();
        }
        repository.save(expenseArea);
    }

    @Override
    public void update(Integer id, ExpenseArea expenseArea) {
        if (expenseArea == null) {
            throw new IllegalStateException();
        }
        if (expenseArea.getId() == null) {
            throw new IllegalStateException();
        }
        if (!id.equals(expenseArea.getId())) {
            throw new IllegalStateException();
        }
        repository.save(expenseArea);
    }

    @Override
    public void deleteById(Integer id) {
        if (id == null) {
            throw new IllegalStateException();
        }
        repository.deleteById(id);
    }
}
