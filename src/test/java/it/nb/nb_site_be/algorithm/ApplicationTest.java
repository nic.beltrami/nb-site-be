package it.nb.nb_site_be.algorithm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Unit test for simple App.
 */
public class ApplicationTest {
    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        int A1[] = {
        		  1,3,1,3,2,1,3
        };
        
        int A[] = {
      		  1,3,1,4,2,3,5,4
        };
        int N = 5;
        System.out.println(this.solution(N, A));
    }
    
	public int solution(int X, int[] A) {
		int res = -1;
		List<Integer> allJumps = Arrays.stream(A).boxed().collect(Collectors.toList());
		
		if (allJumps.isEmpty()) {
			return res;
		}
		
		if (allJumps.size() == 1 && A[0] == X) {
			return 0;
		}
		
		if (allJumps.stream().allMatch(j ->  j == X)) {
			return res;
		}
		
		List<Integer> tmp = new ArrayList<Integer>();
		
		int pos = 0;
		for (Integer value : allJumps) {
			if (value == X) {
				if (tmp.stream().distinct().count() == (X - 1)) {
					if (tmp.size() > 1) {
						List<Integer> tmpSorted = tmp.stream().sorted().collect(Collectors.toList());
						Integer previousValue = null;
						for (Integer i: tmpSorted) {
							if (previousValue == null) {
								previousValue = i;
							} else if (i != (previousValue + 1)){
								break;
							}
						}
						return pos;
					}
				}
				tmp = new ArrayList<Integer>();
			} else {
				tmp.add(value);	
			}
			pos++;
		}
		
		return res;
	}
    



}
