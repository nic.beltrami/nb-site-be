package it.nb.nb_site_be.algorithm;

public class BinaryGap {

	public int solution(int N) {
		int res = 0;
		try {
			String binaryValue = Integer.toBinaryString(N);
			if (this.checkGap(binaryValue)) {
				int resZero = this.countDistance(binaryValue, '1');
				int resOne = this.countDistance(binaryValue, '0');
				res = resZero > resOne ? resZero : resOne;
			}
		} catch (Exception e) {

		}
		return res;

	}

	private int countDistance(String binaryValue, char value) {
		int res = 0;
		int pos = binaryValue.indexOf(value);
		binaryValue = binaryValue.substring(pos + 1, binaryValue.length());

		pos = binaryValue.indexOf(value);
		if (pos > -1) {
			res = pos;
			if (binaryValue.length() > 1) {
				int subRes = this.countDistance(binaryValue, value);
				if (subRes > res) {
					res = subRes;
				}
			}
		}
		return res;
	}

	private boolean checkGap(String binaryValue) {
		boolean existGap = false;
		if (binaryValue.contains("01") && binaryValue.contains("10")) {
			existGap = true;
		}
		return existGap;

	}
}
