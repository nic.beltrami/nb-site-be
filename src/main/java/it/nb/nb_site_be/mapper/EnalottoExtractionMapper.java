package it.nb.nb_site_be.mapper;

import java.util.List;

// import org.mapstruct.Mapper;

import it.nb.nb_site_be.dto.EnalottoExtractionDTO;
import it.nb.nb_site_be.entity.EnalottoExtraction;

// @Mapper(componentModel = "spring")
public interface EnalottoExtractionMapper {

	EnalottoExtractionDTO toDto(EnalottoExtraction enalottoEstraction);

	List<EnalottoExtractionDTO> toDtos(List<EnalottoExtraction> enalottoEstractions);
}
