package it.nb.nb_site_be.service;

import it.nb.nb_site_be.entity.Expense;

public interface ExpenseService {

    void save(Expense expense);

    void update(Integer id, Expense expense);

    void deleteById(Integer id);

}
