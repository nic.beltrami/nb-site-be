package it.nb.nb_site_be.algorithm;

public class FrogJmp {

    
    public int solution(int X, int Y, int D) {
    	
    		if (X == Y) {
    			return 0;
    		}

    		double xpow = Math.pow(Integer.valueOf(X).doubleValue(), 2L);
    		double ypow = Math.pow(Integer.valueOf(Y).doubleValue(), 2L);
    		
    		double sqrt = (ypow > xpow) ? Math.sqrt(ypow - xpow) : Math.sqrt(xpow - ypow);
    		return Double.valueOf(Math.ceil((Double.valueOf(sqrt) / D))).intValue();
    		
    }
    
}
