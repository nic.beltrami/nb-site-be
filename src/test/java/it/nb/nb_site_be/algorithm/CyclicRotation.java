package it.nb.nb_site_be.algorithm;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class CyclicRotation {
    public int[] solution(int[] A, int K) {
    	
        List<Integer> mylist = Arrays.stream(A).boxed().collect(Collectors.toList());
        Collections.rotate(mylist, K); 
        return mylist.stream().mapToInt(i->i).toArray();

    }
}
