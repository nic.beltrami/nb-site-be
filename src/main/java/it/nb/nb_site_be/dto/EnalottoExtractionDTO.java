package it.nb.nb_site_be.dto;

import java.util.List;

public class EnalottoExtractionDTO {

	public String date;
	public List<Integer> numbers;
	public Integer superstar;
	public Integer jolly;

}
