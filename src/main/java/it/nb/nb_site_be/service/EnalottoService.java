package it.nb.nb_site_be.service;

import java.util.List;

import it.nb.nb_site_be.entity.EnalottoExtraction;

public interface EnalottoService {
	List<EnalottoExtraction> list(String year);
}
