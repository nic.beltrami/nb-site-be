package it.nb.nb_site_be.service;

import it.nb.nb_site_be.entity.Expense;
import it.nb.nb_site_be.repository.ExpenseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ExpenseServiceImpl implements ExpenseService {

    ExpenseRepository repository;

    @Autowired
    public ExpenseServiceImpl(ExpenseRepository repository) {
        this.repository = repository;
    }

    @Override
    public void save(Expense expense) {
        if (expense == null) {
            throw new IllegalStateException();
        }
        this.repository.save(expense);
    }

    @Override
    public void update(Integer id, Expense expense) {
        if (expense == null) {
            throw new IllegalStateException();
        }
        if (expense.getId() == null) {
            throw new IllegalStateException();
        }
        if (!id.equals(expense.getId())) {
            throw new IllegalStateException();
        }
        this.repository.save(expense);
    }

    @Override
    public void deleteById(Integer id) {
        if (id == null) {
            throw new IllegalStateException();
        }
        this.repository.deleteById(id);
    }
}
