package it.nb.nb_site_be.controller;

import it.nb.nb_site_be.entity.ExpenseArea;
import it.nb.nb_site_be.service.ExpenseAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/expensearea")
@CrossOrigin
public class ExpenseAreaController {

    ExpenseAreaService expenseAreaService;

    @Autowired
    public ExpenseAreaController(ExpenseAreaService expenseAreaService) {
        this.expenseAreaService = expenseAreaService;
    }

    @GetMapping
    public List<ExpenseArea> findAll() {
        return expenseAreaService.findAll();
    }

    @GetMapping(value = "/{id}")
    public ExpenseArea findById(@PathVariable("id") Integer id) {
        return expenseAreaService.findById(id);
    }

    @PostMapping
    public void insert(@RequestBody ExpenseArea expenseArea) {
        expenseAreaService.save(expenseArea);
    }

    @PutMapping(value = "/{id}")
    public void update(@PathVariable("id") Integer id, @RequestBody ExpenseArea expenseArea) {
        expenseAreaService.update(id, expenseArea);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable("id") Integer id) {
        expenseAreaService.deleteById(id);
    }

}
