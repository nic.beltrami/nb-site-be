package it.nb.nb_site_be.algorithm.sort;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class QuickSortTest {

	@Test
	public void testQuickSort() {
        int arr[] = {10, 7, 8, 9, 1, 5}; 
        int n = arr.length; 
  
        QuickSort ob = new QuickSort(); 
        ob.sort(arr, 0, n-1); 
        
        int result[] = {1, 5, 7, 8, 9, 10}; 
        assertTrue(Arrays.equals(arr, result));
 	}
}
