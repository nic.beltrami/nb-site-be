package it.nb.nb_site_be;

import it.nb.nb_site_be.repository.ExpenseAreaRepository;
import it.nb.nb_site_be.service.ExpenseAreaService;
import it.nb.nb_site_be.service.ExpenseAreaServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;

import static org.mockito.Mockito.*;

@SpringBootTest
public class ExpenseAreaServiceTest {

    @Mock
    private ExpenseAreaRepository repository;

    @InjectMocks
    private ExpenseAreaService service;

    public ExpenseAreaServiceTest() {
        this.service = new ExpenseAreaServiceImpl(repository);
    }

    @BeforeEach
    void setMockOutput() {
        MockitoAnnotations.initMocks(this);
        when(service.findAll()).thenReturn(Collections.emptyList());
    }

    @DisplayName("Test FindAll")
    @Test
    void testFindAll() {
        service.findAll();
        verify(repository, times(1)).findAll();
    }

}
