package it.nb.nb_site_be.algorithm;

import java.util.Arrays;
import java.util.OptionalInt;

public class PermCheck {

    public int solution(int[] A) {
		int value = 0;
		
		if (A.length == 1 && A.length == 2) {
			return value;
		}
		
		OptionalInt max = Arrays.stream(A).max();
		OptionalInt min = Arrays.stream(A).min();
		if (min.isPresent() && max.isPresent() && A.length > 0) {
    		int realCount = ((max.getAsInt() + min.getAsInt()) * A.length) / 2; 
    		if (Arrays.stream(A).sum() == realCount)
    			value = 1;
		}
		return value;
		
}
}
