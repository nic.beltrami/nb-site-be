package it.nb.nb_site_be.repository;

import it.nb.nb_site_be.entity.ExpenseArea;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExpenseAreaRepository extends JpaRepository<ExpenseArea, Integer> {
}
