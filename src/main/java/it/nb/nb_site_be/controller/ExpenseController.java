package it.nb.nb_site_be.controller;

import it.nb.nb_site_be.entity.Expense;
import it.nb.nb_site_be.service.ExpenseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/expense")
@CrossOrigin
public class ExpenseController {

    ExpenseService expenseService;

    @Autowired
    public ExpenseController(ExpenseService expenseService) {
        this.expenseService = expenseService;
    }

    @PostMapping
    public void insert(@RequestBody Expense expense) {
        expenseService.save(expense);
    }

    @PutMapping(value = "/{id}")
    public void update(@PathVariable("id") Integer id, @RequestBody Expense expense) {
        expenseService.update(id, expense);
    }

    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable("id") Integer id) {
        expenseService.deleteById(id);
    }
}
