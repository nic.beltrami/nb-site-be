package it.nb.nb_site_be.algorithm;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class OddOccurrencesInArray {

    public int solution(int[] A) {
		int ret = Integer.MIN_VALUE; 
		IntStream stream = Arrays.stream(A);
		List<Integer> list = stream.boxed().collect(Collectors.toList());
		Map<Integer, Long> map = list.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
		for (Map.Entry<Integer, Long> entry : map.entrySet()) {
			if (entry.getValue() == 1) {
				ret = entry.getKey();
			}
		}
		return ret;
    }
}
