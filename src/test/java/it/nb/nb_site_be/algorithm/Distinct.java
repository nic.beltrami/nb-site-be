package it.nb.nb_site_be.algorithm;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Distinct {
	public int solution(int[] A) {
		List<Integer> mylist = Arrays.stream(A).boxed().collect(Collectors.toList());
		Long count = mylist.stream().distinct().count();
		return count.intValue();
	}
}
