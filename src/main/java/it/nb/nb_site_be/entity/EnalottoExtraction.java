package it.nb.nb_site_be.entity;

import java.util.List;

public class EnalottoExtraction {

	private String date;
	private List<Integer> numbers;
	private Integer superstar;
	private Integer jolly;

	// Fluent
	public EnalottoExtraction date(String date) {
		this.date = date;
		return this;
	}

	public EnalottoExtraction numbers(List<Integer> numbers) {
		this.numbers = numbers;
		return this;
	}

	public EnalottoExtraction superstar(Integer superstar) {
		this.superstar = superstar;
		return this;
	}

	public EnalottoExtraction jolly(Integer jolly) {
		this.jolly = jolly;
		return this;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public List<Integer> getNumbers() {
		return numbers;
	}

	public void setNumbers(List<Integer> numbers) {
		this.numbers = numbers;
	}

	public Integer getSuperstar() {
		return superstar;
	}

	public void setSuperstar(Integer superstar) {
		this.superstar = superstar;
	}

	public Integer getJolly() {
		return jolly;
	}

	public void setJolly(Integer jolly) {
		this.jolly = jolly;
	}

}
