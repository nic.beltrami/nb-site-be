package it.nb.nb_site_be.algorithm.sort;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class MergeSortTest {

	@Test
	public void testQuickSort() {
        int arr[] = {12, 11, 13, 5, 6, 7}; 
     
        MergeSort ob = new MergeSort(); 
        ob.sort(arr, 0, arr.length-1); 
  
        int result[] = {5, 6, 7, 11, 12, 13}; 
        assertTrue(Arrays.equals(arr, result));
 	}
}
