package it.nb.nb_site_be.algorithm;

import java.util.Arrays;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Collectors;

public class PermMissingElement {

	   public int solution(int[] A) {
	    	
	        if (A.length == 0) {
				return 1;
			}
			
			if (A.length == 1) {
				return A[0] + 1;
			}
			
			if (A.length == 2) {
				return A[1] + 1;
			}

			List<Integer> mylist = Arrays.stream(A).sorted().boxed().collect(Collectors.toList());

			OptionalInt max = Arrays.stream(A).max();
			OptionalInt min = Arrays.stream(A).min();
			
			for (int i = min.getAsInt(); i <= max.getAsInt(); i++) {
				if(!mylist.contains(i)) {
					return i;
				}
			}
			
			return 1;
	    }
}
