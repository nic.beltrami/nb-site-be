package it.nb.nb_site_be.algorithm;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class FrogRiverOne {
	public int solution(int X, int[] A) {
		int res = -1;
		List<Integer> allJumps = Arrays.stream(A).boxed().collect(Collectors.toList());
		
		if (allJumps.isEmpty()) {
			return res;
		}
		
		if (allJumps.size() == 1 && A[0] == X) {
			return 0;
		}
		
		if (allJumps.stream().allMatch(j ->  j == X)) {
			return res;
		}
		
		for (Integer i = Arrays.stream(A).min().getAsInt(); i < X; i++) {
			if (!allJumps.contains(i)) {
				return res;
			}
		}
		
		Optional<Integer> minimalJump = allJumps.stream().filter(v -> v == X).findFirst();
		if (!minimalJump.isPresent()) {
			return res;
		}
		return allJumps.indexOf(minimalJump.get());
	}
}
